import React, { useEffect, useState } from 'react';
import { Switch } from '@headlessui/react';
import { HiDownload } from 'react-icons/hi';
import CsvDownloader from 'react-csv-downloader';
import {
  ApiError,
  Fetcher,
  OpArgType,
  TypedFetch,
  FetchReturnType,
  FetchErrorType,
} from 'openapi-typescript-fetch';
import { paths, components } from './s4g_backend_admin';
import config from './config';

const fetcher = Fetcher.for<paths>();

console.log(config);

fetcher.configure({
  baseUrl: config.S4G__BACKEND_URL,
});

const getProvidersFetcher = fetcher
  .path('/api/v1/provider')
  .method('get')
  .create();
const setProviderStatusFetcher = fetcher
  .path('/api/v1/provider/{name}/status')
  .method('put')
  .create();

type ProviderInfo = components['schemas']['ProviderInfo'];
type ProviderOpt = components['schemas']['ProviderOpt'];

type FetchResult<F> =
  | { data: FetchReturnType<F>; status: 'success' }
  | { err: [ApiError, FetchErrorType<F>]; status: 'known_error' }
  | { err: unknown; status: 'unknown_error' };

function wrap<OP>(
  fetcher: TypedFetch<OP>,
): (arg: OpArgType<OP>) => Promise<FetchResult<TypedFetch<OP>>> {
  return async (arg) => {
    try {
      const response = await fetcher(arg);

      return { data: response.data, status: 'success' };
    } catch (e) {
      if (e instanceof ApiError && e instanceof fetcher.Error)
        return { err: [e, e.getActualType()], status: 'known_error' };

      return { err: e, status: 'unknown_error' };
    }
  };
}

const getProviders = wrap(getProvidersFetcher);
const setProviderStatus = wrap(setProviderStatusFetcher);

type AppData = ProviderInfo[];

type MySwitchProps = {
  enabled: boolean;
  onChange: (_: boolean) => void;
};

function MySwitch(props: MySwitchProps) {
  return (
    <Switch
      checked={props.enabled}
      className="
                relative inline-flex h-6 w-11 
                items-center rounded-full 
                ui-checked:bg-gray-200 ui-not-checked:bg-white
            "
      onChange={props.onChange}
    >
      <span className="sr-only">Enable notifications</span>
      <span
        className="
                inline-block h-4 w-4 transform rounded-full
                transition ui-checked:translate-x-6
                ui-checked:bg-gray-500 ui-not-checked:translate-x-1 ui-not-checked:bg-gray-800"
      />
    </Switch>
  );
}

type ProviderProps = {
  info: ProviderInfo;
  onChange: () => Promise<void>;
};

function changeProviderStatus(
  onChange: () => Promise<void>,
  name: ProviderOpt,
  enable: boolean,
) {
  (async () => {
    const response = await setProviderStatus({ enable, name });

    if (response.status == 'success') {
      await onChange();
    } else if (response.status == 'known_error') {
      // todo
    }
  })();
}

function Provider(props: ProviderProps) {
  return (
    <tr
      className={
        props.info.enabled
          ? 'border-b bg-white opacity-100'
          : 'border-b bg-gray-100 opacity-50'
      }
    >
      <td
        className="
                whitespace-nowrap py-4 px-6 text-center font-medium text-gray-900
                "
      >
        {props.info.provider}
      </td>
      <td
        className="
                whitespace-nowrap py-4 px-6 text-center font-medium text-gray-600
                "
      >
        {props.info.url_conversions}
      </td>
      <td
        className="
                py-4 px-6 text-center
                "
      >
        <MySwitch
          enabled={props.info.enabled}
          onChange={(on) =>
            changeProviderStatus(props.onChange, props.info.provider, on)
          }
        />
      </td>
    </tr>
  );
}

type ProvidersProps = {
  appData: AppData;
  children: React.ReactNode;
};

function Providers({ appData, children }: ProvidersProps) {
  return (
    <div className="overflow-x-auto rounded-lg shadow-md">
      <table className="w-full text-gray-500">
        <thead className="bg-gray-200 text-gray-700">
          <tr className="border-b">
            <th className="py-3 px-3 text-center text-gray-800" scope="col">
              {' '}
              Provider{' '}
            </th>
            <th className="py-3 px-3 text-center text-gray-800" scope="col">
              {' '}
              Shortened Urls{' '}
            </th>
            <th className="text-center" scope="col">
              <CsvDownloader
                datas={() => stringifyApp(appData)}
                filename="report.csv"
              >
                <button
                  aria-label="Customise options"
                  className="
                                         inline-flex h-8 w-8 items-center justify-center
                                         rounded-full 
                                         bg-gray-500 text-white
                                         active:scale-95
                                    "
                >
                  <HiDownload />
                </button>
              </CsvDownloader>
            </th>
          </tr>
        </thead>
        <tbody>{children}</tbody>
      </table>
    </div>
  );
}

async function fetchAndUpdateData(setData: (_: AppData) => void) {
  const response = await getProviders({});

  if (response.status == 'success') {
    setData(response.data);
  }
}

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

function fetchAndUpdateDataLoop(
  stopToken: boolean,
  setState: (_: AppData) => void,
) {
  (async () => {
    while (!stopToken) {
      await fetchAndUpdateData(setState);

      await sleep(1000);
    }
  })();
}

type Stringify<T> = {
  [K in keyof T]: T[K] extends object | null ? Stringify<T[K]> : string;
};

function stringifyInfo(x: ProviderInfo): Stringify<ProviderInfo> {
  return {
    enabled: x.enabled.toString(),
    provider: x.provider,
    url_conversions: x.url_conversions.toString(),
  };
}

function stringifyApp(x: AppData): Stringify<AppData> {
  return x.map(stringifyInfo);
}

export default function App() {
  const [data, setData] = useState<AppData>([]);

  useEffect(() => {
    let stopToken = false;
    fetchAndUpdateDataLoop(stopToken, setData);

    return () => {
      stopToken = true;
    };
  }, []);

  const providers = data.map((info) => {
    return (
      <Provider
        info={info}
        key={info.provider}
        onChange={() => fetchAndUpdateData(setData)}
      />
    );
  });

  return (
    <div className="mx-auto my-20 w-8/12">
      <Providers appData={data}>{providers}</Providers>
    </div>
  );
}
