/**
 * This file was auto-generated by openapi-typescript.
 * Do not make direct changes to the file.
 */

export interface paths {
  '/api/v1/provider': {
    get: operations['index'];
  };
  '/api/v1/provider/{name}': {
    get: operations['show'];
  };
  '/api/v1/provider/{name}/status': {
    put: operations['status'];
  };
}

export interface components {
  schemas: {
    ProviderInfo: {
      enabled: boolean;
      provider: components['schemas']['ProviderOpt'];
      /**
       * Format: int32
       * @example 1234
       */
      url_conversions: number;
    };
    /** @enum {string} */
    ProviderOpt: 'bitly' | 'tinyurl';
    ProviderSetStatusRequest: {
      enable: boolean;
    };
  };
}

export interface operations {
  index: {
    responses: {
      /** List supported Providers and relevant statistics. */
      200: {
        content: {
          'application/json': components['schemas']['ProviderInfo'][];
        };
      };
    };
  };
  show: {
    parameters: {
      path: {
        name: components['schemas']['ProviderOpt'];
      };
    };
    responses: {
      /** Retrieve Provider information. */
      200: {
        content: {
          'application/json': components['schemas']['ProviderInfo'];
        };
      };
      /** Malformed path parameter. */
      400: {
        content: {
          'text/plain': string;
        };
      };
    };
  };
  status: {
    parameters: {
      path: {
        name: components['schemas']['ProviderOpt'];
      };
    };
    requestBody: {
      content: {
        'application/json': components['schemas']['ProviderSetStatusRequest'];
      };
    };
    responses: {
      /** Successfully changed Provider status. */
      200: unknown;
      /** Malformed path parameter or request body. */
      400: {
        content: {
          'text/plain': string;
        };
      };
    };
  };
}
