# S4G Url Shortener Admin Panel.

Minimal admin panel for the [url shortener service](https://gitlab.com/s4g_url_shortener/s4g_backend).

- Real-time provider reporting
- Downloadable csv reports
- Provider enabling/disabling

# Api Type Safety

We use the [service](https://gitlab.com/s4g_url_shortener/s4g_backend)-provided OpenAPI definitions for generating a type-safe fetch interface
using [openapi-typescript](https://www.npmjs.com/package/openapi-typescript) and
[openapi-typescript-fetch](https://www.npmjs.com/package/openapi-typescript-fetch).

# Observability

The application is compatible with the [opentelemetry](https://opentelemetry.io/) standard and
supports distributed tracing for the user interactions and Fetch calls.

# Configuration

We follow the [12 Factor App](https://12factor.net) guidelines for configuration.
The following environment variables are supported:

- `S4G__BACKEND__URL`: The [url shortener service](https://gitlab.com/s4g_url_shortener/s4g_backend)'s URL.

We also provide some minimal [opentelemetry](https://opentelemetry.io/docs/reference/specification/sdk-environment-variables/) support through:

- `S4G__OTEL_RESOURCE_ATTRIBUTES_SERVICE_NAMESPACE`
- `S4G__OTEL_RESOURCE_ATTRIBUTES_SERVICE_NAME`
- `S4G__OTEL_EXPORTER_OTLP_ENDPOINT`

Due to the project producing a static bundle that will run on the browser, we have no actual support for environment variables during runtime.

Our nginx-powered [Docker image](#docker) supports runtime configuration through config file substitution on nginx startup.

During development we can use environment variables due to `vite` running a local webserver.

The project also supports gitignored-`.env` files at the project root for easier development.

# Workflow

- `npm run install` after cloning.
- `npm run serve` for local development.
- `npm run lint` for linting.
- `npx prettier . --check -w` for formating.
- `npm run build` for production bundling.

# Docker

In order to support container orchestration workflows and platform-agnostic deployments, we provide an nginx-powered image.

In order to make our [Docker image](#docker) "build-once, run anywhere", we use a manual chunked `src/config.ts` file which maintains the configuration. During build time we maintain the env variable names by setting them to themselves and then bundle an `envsubst` script that runs at nginx start and replaces the variables within the config file with the runtime values.

A concrete example of using the Docker images can be found at the [demo repository](https://gitlab.com/s4g_url_shortener/s4g_demo).
