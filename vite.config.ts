import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    rollupOptions: {
      manualChunks: {
        config: ['src/config.ts'],
      },
    },
  },
  envPrefix: 'S4G__',
  plugins: [react()],
});
