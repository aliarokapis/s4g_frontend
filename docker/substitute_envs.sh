#!/bin/sh

set -e

CUR_DIR=`pwd`

cd /var/www/assets

for file in config.*.js;
do
    envsubst < $file > /tmp/$file.subst
    mv /tmp/$file.subst $file
done

cd $CUR_DIR
