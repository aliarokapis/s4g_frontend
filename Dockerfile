FROM node:lts-alpine3.16 AS builder

COPY package.json .

RUN npm install

# hack to retain environment variables for later substitution

ENV S4G__OTEL_RESOURCE_ATTRIBUTES_SERVICE_NAMESPACE='$S4G__OTEL_RESOURCE_ATTRIBUTES_SERVICE_NAMESPACE'
ENV S4G__OTEL_RESOURCE_ATTRIBUTES_SERVICE_NAME='$S4G__OTEL_RESOURCE_ATTRIBUTES_SERVICE_NAME'
ENV S4G__OTEL_EXPORTER_OTLP_ENDPOINT='$S4G__OTEL_EXPORTER_OTLP_ENDPOINT'
ENV S4G__BACKEND_URL='$S4G__BACKEND_URL'

COPY . .

RUN npm run build && \
    mkdir -p /var && \
    mv dist /var/www

FROM nginx:1.23.2-alpine as runtime

COPY --from=builder /var/www /var/www

COPY docker/substitute_envs.sh /docker-entrypoint.d/
COPY docker/default.conf.template /etc/nginx/templates/default.conf.template
